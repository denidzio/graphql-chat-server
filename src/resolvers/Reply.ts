import { Context } from "graphql-yoga/dist/types";

function message(parent: any, args: any, context: Context) {
  return context.prisma.reply({ id: parent.id }).message();
}

export default { message };
