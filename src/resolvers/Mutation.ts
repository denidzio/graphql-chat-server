import { Context } from "graphql-yoga/dist/types";

function sendMessage(parent: any, args: any, context: Context, info: any) {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
    dislikes: 0,
  });
}

async function replyMessage(
  parent: any,
  args: any,
  context: Context,
  info: any
) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.createReply({
    text: args.text,
    message: { connect: { id: args.messageId } },
    likes: 0,
    dislikes: 0,
  });
}

async function likeMessage(
  parent: any,
  args: any,
  context: Context,
  info: any
) {
  const messageToUpdate = await context.prisma.message({
    id: args.id,
  });

  if (!messageToUpdate) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.updateMessage({
    data: {
      likes: messageToUpdate.likes + 1,
    },
    where: {
      id: args.id,
    },
  });
}

async function dislikeMessage(
  parent: any,
  args: any,
  context: Context,
  info: any
) {
  const messageToUpdate = await context.prisma.message({
    id: args.id,
  });

  if (!messageToUpdate) {
    throw new Error(`Message with ID ${args.messageId} does not exist`);
  }

  return context.prisma.updateMessage({
    data: { dislikes: messageToUpdate.dislikes + 1 },
    where: { id: args.id },
  });
}

async function likeReply(parent: any, args: any, context: Context, info: any) {
  const replyToUpdate = await context.prisma.reply({ id: args.id });

  if (!replyToUpdate) {
    throw new Error(`Reply with ID ${args.messageId} does not exist`);
  }

  return context.prisma.updateReply({
    data: { likes: replyToUpdate.likes + 1 },
    where: { id: args.id },
  });
}

async function dislikeReply(
  parent: any,
  args: any,
  context: Context,
  info: any
) {
  const replyToUpdate = await context.prisma.reply({ id: args.id });

  if (!replyToUpdate) {
    throw new Error(`Reply with ID ${args.messageId} does not exist`);
  }

  return context.prisma.updateReply({
    data: { dislikes: replyToUpdate.dislikes + 1 },
    where: { id: args.id },
  });
}

export default {
  sendMessage,
  replyMessage,
  likeMessage,
  dislikeMessage,
  likeReply,
  dislikeReply,
};
