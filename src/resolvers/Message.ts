import { Context } from "graphql-yoga/dist/types";

function replies(parent: any, args: any, context: Context) {
  return context.prisma.message({ id: parent.id }).replies();
}

export default { replies };
