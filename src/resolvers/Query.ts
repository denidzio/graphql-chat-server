import { Context } from "graphql-yoga/dist/types";

async function message(parent: any, args: any, context: Context) {
  return await context.prisma.message({ id: args.id });
}

async function messages(parent: any, args: any, context: Context) {
  const where = args.filter ? { text_contains: args.filter } : {};

  const list = await context.prisma.messages({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy,
  });

  const count = await context.prisma
    .messagesConnection({ where })
    .aggregate()
    .count();

  return { list, count };
}

export default { messages, message };
