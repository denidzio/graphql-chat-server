import { Context } from "graphql-yoga/dist/types";

const newMessageSubscribe = (
  parent: any,
  args: any,
  context: Context,
  info: any
) => context.prisma.$subscribe.message({ mutation_in: ["CREATED"] }).node();

const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: (payload: any) => payload,
};

const updateMessageSubscribe = (
  parent: any,
  args: any,
  context: Context,
  info: any
) => context.prisma.$subscribe.message({ mutation_in: ["UPDATED"] }).node();

const updateMessage = {
  subscribe: updateMessageSubscribe,
  resolve: (payload: any) => payload,
};

const newReplySubscribe = (
  parent: any,
  args: any,
  context: Context,
  info: any
) => context.prisma.$subscribe.reply({ mutation_in: ["CREATED"] }).node();

const newReply = {
  subscribe: newReplySubscribe,
  resolve: (payload: any) => payload,
};

const updateReplySubscribe = (
  parent: any,
  args: any,
  context: Context,
  info: any
) => context.prisma.$subscribe.reply({ mutation_in: ["UPDATED"] }).node();

const updateReply = {
  subscribe: updateReplySubscribe,
  resolve: (payload: any) => payload,
};

export default { newMessage, updateMessage, newReply, updateReply };
