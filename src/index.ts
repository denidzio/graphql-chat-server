import express from "express";
import { GraphQLServer } from "graphql-yoga";
import path from "path";
import { prisma } from "./generated/prisma-client";
import * as resolvers from "./resolvers";

const STATIC = path.join(__dirname, "public");

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: { prisma },
});

server.express.use(express.static(STATIC));

server.start(() => console.log("Server has been started"));
